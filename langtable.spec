# This spec is from community: https://github.com/mike-fabian/langtable/blob/master/langtable.spec

Name:           langtable
Version:        0.0.64
Release:        1
Summary:        Guessing reasonable defaults for locale, keyboard layout, territory, and language.
Group:          Development/Tools
# the translations in languages.xml and territories.xml are (mostly)
# imported from CLDR and are thus under the Unicode license, the
# short name for this license is "MIT"
License:        GPLv3+
URL:            https://github.com/mike-fabian/langtable
Source0:        https://github.com/mike-fabian/langtable/releases/download/%{version}/%{name}-%{version}.tar.gz
Patch9000:      0001-langtable-add-Beijing-timezone-and-delete-others-lan.patch
Patch9001:      0002-langtable-remove-failure-exception-thrown-in-test_cases.patch
Patch9002:      0003-langtable-modify-Simplified-Chinese-usage-area.patch

BuildArch:      noarch
BuildRequires:  perl-interpreter
BuildRequires:  python3-devel

%description
langtable is used to guess reasonable defaults for locale, keyboard layout,
territory, and language, if part of that information is already known. For
example, guess the territory and the keyboard layout if the language
is known or guess the language and keyboard layout if the territory is
already known.

%package -n python3-langtable
Summary:        Python module to query the langtable-data
Group:          Development/Tools
License:        GPLv3+
Requires:       %{name} = %{version}-%{release}
Obsoletes:      %{name}-data < %{version}-%{release}
Provides:       %{name}-data = %{version}-%{release}
%{?python_provide:%python_provide python3-%{name}}

%description -n python3-langtable
This package contains a Python module to query the data
from langtable-data.

%prep
%setup -q
gzip -d langtable/data/*.xml.gz
%patch9000 -p1
%patch9001 -p1
%patch9002 -p1
gzip --force --best langtable/data/*.xml

%build
perl -pi -e "s,_DATADIR = '(.*)',_DATADIR = '%{_datadir}/langtable'," langtable/langtable.py

%py3_build

%install
%py3_install

%check
(cd $RPM_BUILD_DIR/%{name}-%{version}/langtable; %{__python3} langtable.py)
(cd $RPM_BUILD_DIR/%{name}-%{version}; %{__python3} test_cases.py)
xmllint --noout --relaxng $RPM_BUILD_DIR/%{name}-%{version}/langtable/schemas/keyboards.rng $RPM_BUILD_DIR/%{name}-%{version}/langtable/data/keyboards.xml.gz
xmllint --noout --relaxng $RPM_BUILD_DIR/%{name}-%{version}/langtable/schemas/languages.rng $RPM_BUILD_DIR/%{name}-%{version}/langtable/data/languages.xml.gz
xmllint --noout --relaxng $RPM_BUILD_DIR/%{name}-%{version}/langtable/schemas/territories.rng $RPM_BUILD_DIR/%{name}-%{version}/langtable/data/territories.xml.gz
xmllint --noout --relaxng $RPM_BUILD_DIR/%{name}-%{version}/langtable/schemas/timezoneidparts.rng $RPM_BUILD_DIR/%{name}-%{version}/langtable/data/timezoneidparts.xml.gz
xmllint --noout --relaxng $RPM_BUILD_DIR/%{name}-%{version}/langtable/schemas/timezones.rng $RPM_BUILD_DIR/%{name}-%{version}/langtable/data/timezones.xml.gz

%files
%license COPYING unicode-license.txt
%doc README ChangeLog test_cases.py langtable/schemas/*.rng

%files -n python3-langtable
%dir %{python3_sitelib}/langtable
%{python3_sitelib}/langtable/*
%dir %{python3_sitelib}/langtable-*.egg-info
%{python3_sitelib}/langtable-*.egg-info/*

%changelog
* Mon Jan 22 2024 gengqihu <gengqihu2@h-partners.com>- 0.0.64-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.0.64

* Sat Jan 28 2023 zhangruifang2020 <zhangruifang1@h-partners.com> - 0.0.61-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.0.61

* Tue Aug 23 2022 hubin <hubin73@huawei.com> - 0.0.56-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify Simplified Chinese usage area

* Thu Dec 30 2021 wangchen <wangchen137@huawei.com> - 0.0.56-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.0.56

* Thu Jan 28 2021 wangchen <wangchen137@huawei.com> - 0.0.54-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.0.54

* Mon Jul 6 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 0.0.51-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add Beijing timezone and delete others languages

* Tue Jun 16 2020 zhangqiumiao <zhangqiumiao1@huawei.com> - 0.0.51-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to version 0.0.51
 
* Tue May 19 2020 songzhenyu <songzhenyu@huawei.com> - 0.0.38-9
- modify release

* Mon Oct 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.0.38-8.h7
- update spec info

* Tue Aug 20 2019 fangyufa<fangyufa1@huawei.com> - 0.0.38-8.h6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify name of patch

* Fri Aug 09 2019 fangyufa<fangyufa1@huawei.com> - 0.0.38-8.h5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: modify info of patch

* Thu Aug 08 2019 fangyufa<fangyufa1@huawei.com> - 0.0.38-8.h4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: add reason of patch

* Mon Aug 05 2019 zhuguodong <zhuguodong7@huawei.com> - 0.0.38-8.h3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: openEuler Debranding

* Mon Feb 11 2019 cangyi<cangyi@huawei.com> - 0.0.38-8.h2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add Beijing timezone and delete others languages

* Mon Jan 28 2019 Shouping Wang<wangshouping@huawei.com> - 0.0.38-8.h1
- Type:enhancement
- ID:NA
- SUG:restart
- DESC:build python2-langtable

* Tue Jul 17 2018 zhuguodong <zhuguodong7@huawei.com> - 0.0.38-8
- Package  Initialization
